package com.risk;

import java.util.List;

public class Util
{
    public static float[] convert(List<Float> floats, int size)
    {
        float[] ret = new float[size];
        for (int i = 0; i < ret.length; i++)
            ret[i] = floats.get(i).floatValue();
        return ret;
    }
}
