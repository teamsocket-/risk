package com.risk;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.risk.rendering.WorldRenderer;
import com.risk.geojson.WorldLoader;

public class Risk implements ApplicationListener
{

    private WorldLoader worldLoader;


    PolygonSpriteBatch polyBatch;
    private ShapeRenderer shapeRenderer;

    private WorldRenderer wmr;

    public Risk()
    {
    }

    @Override
    public void create()
    {
        (worldLoader = new WorldLoader()).load();
        wmr = worldLoader.getWorldRenderer();

        polyBatch = new PolygonSpriteBatch();
        shapeRenderer = new ShapeRenderer();
    }

    @Override
    public void dispose()
    {
        polyBatch.dispose();
        shapeRenderer.dispose();
    }

    @Override
    public void render()
    {
        Gdx.gl.glClearColor(1, 1, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        wmr.draw(polyBatch, shapeRenderer);
    }

    @Override
    public void resize(int width, int height)
    {
    }

    @Override
    public void pause()
    {
    }

    @Override
    public void resume()
    {
    }
}
