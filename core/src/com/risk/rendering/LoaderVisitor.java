package com.risk.rendering;

import com.risk.geojson.GeoJsonCountry;
import com.risk.geojson.MultiPolygonBorderDrawer;
import com.risk.geojson.PolygonBorderDrawer;
import org.geojson.*;

import java.util.ArrayList;
import java.util.List;

public class LoaderVisitor implements GeoJsonObjectVisitor<GeoJsonObject>
{

    private String lastFoundFeaturename;

    private final List<GeoJsonCountry> countries;

    public LoaderVisitor()
    {
        countries = new ArrayList<>();
    }

    @Override
    public GeoJsonObject visit(GeometryCollection geoJsonObject)
    {
        return null;
    }

    @Override
    public GeoJsonObject visit(FeatureCollection featureCollection)
    {
        for (Feature feature : featureCollection)
            feature.accept(this);
        return featureCollection;
    }

    @Override
    public GeoJsonObject visit(Point geoJsonObject)
    {
        throw new UnsupportedOperationException("GeoJson Point is not supported");
    }

    @Override
    public GeoJsonObject visit(Feature feature)
    {
        lastFoundFeaturename = feature.getProperty("name");
        if (lastFoundFeaturename == null)
        {
            System.out.println("WARNING: The feature " + feature.getId() + " has no name, using its id for its name " +
                    "instead.");
            lastFoundFeaturename = feature.getId();
        }

        feature.getGeometry().accept(this);

        return feature;
    }

    @Override
    public GeoJsonObject visit(MultiLineString geoJsonObject)
    {
        throw new UnsupportedOperationException("GeoJson MultiLineString is not supported");
    }

    @Override
    public GeoJsonObject visit(Polygon polygon)
    {
        countries.add(new GeoJsonCountry(lastFoundFeaturename, new PolygonBorderDrawer(polygon)));
        return polygon;
    }

    @Override
    public GeoJsonObject visit(MultiPolygon multiPolygon)
    {
        countries.add(new GeoJsonCountry(lastFoundFeaturename, new MultiPolygonBorderDrawer(multiPolygon)));
        return multiPolygon;
    }

    @Override
    public GeoJsonObject visit(MultiPoint geoJsonObject)
    {
        throw new UnsupportedOperationException("GeoJson MultiPoint is not supported");
    }

    @Override
    public GeoJsonObject visit(LineString geoJsonObject)
    {
        throw new UnsupportedOperationException("GeoJson LineString is not supported");
    }

    public List<GeoJsonCountry> getCountries()
    {
        return countries;
    }
}
