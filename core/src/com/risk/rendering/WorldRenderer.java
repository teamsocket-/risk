package com.risk.rendering;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.ArrayList;
import java.util.List;

public class WorldRenderer
{

    private final List<CountryRenderInfo> countries;


    public WorldRenderer()
    {
        countries = new ArrayList<>();
    }

    public void addCountry(CountryRenderInfo cri)
    {
        countries.add(cri);
    }

    public void draw(PolygonSpriteBatch psb, ShapeRenderer sr)
    {
        psb.begin();
        for (CountryRenderInfo cri : countries)
        {
            cri.drawPolybatch(psb);
//            poly.rotate(1.0f);
        }
        psb.end();

        for (CountryRenderInfo cri : countries)
        {
            cri.drawCenter(sr);
        }
    }
}
