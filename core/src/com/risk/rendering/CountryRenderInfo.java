package com.risk.rendering;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.risk.Util;
import org.apache.commons.lang3.ArrayUtils;

import java.util.Arrays;
import java.util.List;

public class CountryRenderInfo
{

    private final String name;
    private final PolygonSprite[] polies;
    private final float centerX, centerY;
    private final List<float[]> borders;

    CountryRenderInfo(String name, List<float[]> borders, PolygonSprite[] polies, float centerX, float centerY)
    {
        this.name = name;
        this.borders = borders;
        this.polies = polies;
        this.centerX = centerX;
        this.centerY = centerY;
    }

    public void drawPolybatch(PolygonSpriteBatch batch)
    {
        for (PolygonSprite poly : polies)
        {
            poly.draw(batch);

            // Turn on for trippy effect
//            poly.scale(0.01f);
//            poly.rotate(0.1f);
        }
    }

    public void drawCenter(ShapeRenderer sr)
    {
        sr.begin(ShapeRenderer.ShapeType.Filled);
        sr.setColor(Color.CYAN);
        sr.circle(centerX, centerY, 2);
        sr.end();

        sr.begin(ShapeRenderer.ShapeType.Line);
        sr.setColor(0, 0, 0, 100);
        for (float[] border : borders)
        {
            sr.polygon(border);
        }
        sr.end();
    }

}
