package com.risk.rendering;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.PolygonRegion;
import com.badlogic.gdx.graphics.g2d.PolygonSprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.EarClippingTriangulator;
import com.badlogic.gdx.utils.ShortArray;
import com.risk.geojson.ICountryDrawerVisitor;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class CountryDrawer implements ICountryDrawerVisitor
{

    private Random r;
    private Color c;
    private Pixmap pix;
    private int screenWidth, screenHeight;

    private int polyCount;
    private List<PolygonSprite> polies;
    private String name;
    private float centerX, centerY;
    private List<float[]> borders;

    public CountryDrawer()
    {
        polyCount = 0;
        polies = new ArrayList<>();
        borders = new ArrayList<>();
        r = new Random();
        c = new Color(r.nextInt());

        screenWidth = Gdx.graphics.getWidth();
        screenHeight = Gdx.graphics.getHeight();

        pix = new Pixmap(1, 1, Pixmap.Format.RGBA8888);
    }

    @Override
    public int getMapWidth()
    {
        return screenWidth;
    }

    @Override
    public int getMapHeight()
    {
        return screenHeight;
    }

    @Override
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public void setCenter(float x, float y)
    {
        this.centerX = x;
        this.centerY = y;
    }

    @Override
    public void polygon(float[] coords)
    {
        borders.add(coords);

        pix.setColor(c);
        pix.fill();
        Texture textureSolid = new Texture(pix);

        EarClippingTriangulator triangulator = new EarClippingTriangulator();
        ShortArray triangleIndices = triangulator.computeTriangles(coords);

        PolygonRegion polyReg = new PolygonRegion(new TextureRegion(textureSolid), coords, triangleIndices.toArray());
        PolygonSprite poly = new PolygonSprite(polyReg);
        poly.setOrigin(centerX, centerY);
        polyCount++;
        polies.add(poly);
    }

    public CountryRenderInfo getCountryRenderInfo()
    {
        CountryRenderInfo cri = new CountryRenderInfo(name, new ArrayList<>(borders), polies.toArray
        (new
                PolygonSprite[polyCount]),
                centerX,
                centerY);

        name = null;
        polyCount = 0;
        polies.clear();
        borders.clear();
        c = new Color(new Random().nextInt());

        return cri;
    }
}
