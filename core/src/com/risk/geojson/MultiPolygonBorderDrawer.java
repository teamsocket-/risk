package com.risk.geojson;

import org.geojson.LngLatAlt;
import org.geojson.MultiPolygon;

import java.util.ArrayList;
import java.util.List;

public class MultiPolygonBorderDrawer extends AbstractBorderDrawer
{

    private final MultiPolygon polygon;

    public MultiPolygonBorderDrawer(MultiPolygon polygon)
    {
        this.polygon = polygon;
    }

    @Override
    public void accept(ICountryDrawerVisitor visitor)
    {
        super.accept(visitor);

        List<float[]> islands = new ArrayList<>();

        for (List<List<LngLatAlt>> island : polygon.getCoordinates())
            islands.add(mercatorProject(island, visitor.getMapWidth(), visitor.getMapHeight()));

        visitor.setCenter(getXCenter(), getYCenter());
        for (float[] island : islands)
            visitor.polygon(island);
    }
}
