package com.risk.geojson;

public interface ICountryDrawerVisitor
{

    int getMapWidth();

    int getMapHeight();

    void setName(String name);

    void setCenter(float x, float y);

    void polygon(float[] coords);
}
