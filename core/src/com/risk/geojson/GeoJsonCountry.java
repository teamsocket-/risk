package com.risk.geojson;

import com.risk.geojson.AbstractBorderDrawer;
import com.risk.geojson.ICountryDrawerVisitor;

public class GeoJsonCountry
{

    private final AbstractBorderDrawer borderDrawer;
    private final String name;

    public GeoJsonCountry(String name, AbstractBorderDrawer borderDrawer)
    {
        this.name = name;
        this.borderDrawer = borderDrawer;
    }

    public String getName()
    {
        return name;
    }

    public void accept(ICountryDrawerVisitor visitor)
    {
        borderDrawer.accept(visitor);
    }
}
