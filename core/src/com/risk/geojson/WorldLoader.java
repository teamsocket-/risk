package com.risk.geojson;

import com.badlogic.gdx.Gdx;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.risk.rendering.CountryDrawer;
import com.risk.rendering.LoaderVisitor;
import com.risk.rendering.WorldRenderer;
import org.geojson.FeatureCollection;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class WorldLoader
{

    private LoaderVisitor loaderVisitor;

    public void load()
    {

        InputStream is = Gdx.files.local("/core/assets/countries.geo.json").read();

        loaderVisitor = new LoaderVisitor();

        try
        {
            new ObjectMapper().readValue(is, FeatureCollection.class).accept(loaderVisitor);

        } catch (IOException e)
        {
            e.printStackTrace();
        }

    }

    public WorldRenderer getWorldRenderer()
    {
        List<GeoJsonCountry> geoCountries = loaderVisitor.getCountries();

        CountryDrawer glCountryDrawer = new CountryDrawer();

        WorldRenderer worldRenderer = new WorldRenderer();

        for (GeoJsonCountry geoCountry : geoCountries)
        {
            geoCountry.accept(glCountryDrawer);
            worldRenderer.addCountry(glCountryDrawer.getCountryRenderInfo());
        }

        return worldRenderer;
    }
}
