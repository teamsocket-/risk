package com.risk.geojson;

import org.geojson.Polygon;

public class PolygonBorderDrawer extends AbstractBorderDrawer
{

    private final Polygon polygon;

    public PolygonBorderDrawer(Polygon polygon)
    {
        this.polygon = polygon;
    }

    @Override
    public void accept(ICountryDrawerVisitor visitor)
    {
        super.accept(visitor);

        float[] projection = mercatorProject(polygon.getCoordinates(), visitor.getMapWidth(), visitor.getMapHeight());
        visitor.setCenter(getXCenter(), getYCenter());
        visitor.polygon(projection);
    }
}
