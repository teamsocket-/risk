package com.risk.geojson;

import com.risk.Util;
import org.geojson.LngLatAlt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class AbstractBorderDrawer
{

    private float minX, maxX, minY, maxY;

    public void accept(ICountryDrawerVisitor visitor)
    {
        resetCenterTracking();
    }

    private void resetCenterTracking()
    {
        minX = Float.MAX_VALUE;
        maxX = Float.MIN_VALUE;
        minY = Float.MAX_VALUE;
        maxY = Float.MIN_VALUE;
    }

    protected float[] mercatorProject(List<List<LngLatAlt>> coords, int mapWidth, int mapHeight)
    {

        if (coords.isEmpty())
        {
            throw new IllegalStateException("Can't convert nothing!");
        }

        int size = 0;

        List<Float> result = new ArrayList<>();
        Iterator<LngLatAlt> polygonIterator = coords.get(0).iterator();

        while (polygonIterator.hasNext())
        {
            LngLatAlt pos = polygonIterator.next();

            float x = (float) ((pos.getLongitude() + 180) * (mapWidth / 360));
            double latRad = pos.getLatitude() * Math.PI / 180;

            double mercN = Math.log(Math.tan((Math.PI / 4) + (latRad / 2)));
            float y = mapHeight - (float) ((mapHeight / 2) - (mapWidth * mercN / (2 * Math.PI)));

            if (x < minX) minX = x;
            if (x > maxX) maxX = x;
            if (y < minY) minY = y;
            if (y > maxY) maxY = y;

            result.add(x);
            result.add(y);

            size += 2;
        }

        return Util.convert(result, size);
    }

    protected float getXCenter()
    {
        return (maxX - minX) / 2 + minX;
    }

    protected float getYCenter()
    {
        return (maxY - minY) / 2 + minY;
    }
}
